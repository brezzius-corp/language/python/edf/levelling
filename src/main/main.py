#!/usr/bin/env python3

"""Archive levelling data from EDF"""

import http.client as chttp
import datetime as dt
import os
import sys
import configparser
import json
import mariadb


def parse(data, conn):
    """Parse data"""

    cursor = conn.cursor()

    txt = json.loads(data)

    for item in reversed(txt['chroniques']):
        sql = 'INSERT INTO levelling (station, date, value) VALUES (%s, %s, %s)'
        val = ('1', dt.datetime.strptime(item['date'], '%d-%m-%Y').strftime('%Y-%m-%d') + ' ' + item['heure'] + ':00', str(item['valeur']))

        cursor.execute(sql, val)

    conn.commit()

    cursor.close()


def validate(sdate):
    """"Validate date format"""

    fdate = '%d-%m-%Y'

    try:
        dt.datetime.strptime(sdate, fdate)
    except ValueError:
        print('Incorrect date format, should be DD-MM-YYYY\n')
        raise ValueError()


def usage():
    """Script usage"""

    print("Usage: main.py [OPTION]\n\nOptions:")
    print(" -h, --help\t\tShow help")
    print(" -d, --date\t\tArchive specific date (DD-MM-YYYY)")
    print(" -f, --full\t\tArchive all dates")

    sys.exit(1)


def main():
    """Main function"""

    if (len(sys.argv) == 3) and ((sys.argv[1] == '--date') or (sys.argv[1] == '-d')):
        cdate = sys.argv[2]
        try:
            validate(cdate)
        except ValueError:
            usage()

    elif (len(sys.argv) == 2) and ((sys.argv[1] == '--full') or (sys.argv[1] == '-f')):
        cdate = None
    elif (len(sys.argv) == 2) and ((sys.argv[1] == '--help') or (sys.argv[1] == '-h')):
        usage()
    elif len(sys.argv) == 1:
        ldate = dt.datetime.today().date() - dt.timedelta(days=1)
        cdate = ldate.strftime('%d-%m-%Y')
    else:
        usage()

    config = configparser.ConfigParser()
    config.read(os.path.dirname(sys.argv[0]) + '/config.ini')

    try:
        conn = mariadb.connect(
            host=config['database']['host'],
            user=config['database']['user'],
            password=config['database']['password'],
            database=config['database']['database']
        )

    except mariadb.Error as error:
        print(f"Error connecting to MariaDB Platform: {error}")
        sys.exit(1)

    station = config['station']['id']

    if cdate is not None:
        name = config['server']['pathname'].format(station, cdate, 0)
    else:
        dt1 = dt.date(2020, 1, 1)
        dt2 = dt.datetime.today().date() - dt.timedelta(days=1)
        dtd = dt2 - dt1
        name = config['server']['pathname'].format(station, dt2.strftime("%d-%m-%Y"), dtd.days)

    cnx = chttp.HTTPSConnection(config['server']['hostname'])
    cnx.request("GET", name, headers={'Laetis': 'Basic ' + config['server']['token']})

    response = cnx.getresponse()
    data = response.read()

    if response.status == 200 and response.reason == 'OK':
        parse(data, conn)

    cnx.close()

    conn.close()


if __name__ == "__main__":
    main()
